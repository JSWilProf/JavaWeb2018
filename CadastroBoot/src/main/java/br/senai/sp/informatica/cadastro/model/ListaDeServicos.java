package br.senai.sp.informatica.cadastro.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListaDeServicos {
	private List<Servico> servicos;
	private int[] ids;
	private int idCliente;
}
