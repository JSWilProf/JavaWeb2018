package br.senai.sp.informatica.cadastro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="users")
public class Usuario {
	@Id
	@Column(name="username")
	private String nome;
	@Transient
	private String old_nome = "";
	@Column(name="password")
	private String senha;
	@Column(name="enabled")
	private boolean habilitado = true;
	@Transient
	private boolean administrador;
	@Transient
	private boolean editar;
	
	public void setSenha(String senha) {
		this.senha = new BCryptPasswordEncoder().encode(senha);
	}
	
	public void mantemSenha(String hash) {
		this.senha = hash;
	}
}
