package br.senai.sp.informatica.cadastro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.component.SecurityCheck;
import br.senai.sp.informatica.cadastro.model.Usuario;
import br.senai.sp.informatica.cadastro.service.UsuarioService;

@Controller
public class UsuarioController {
	@Autowired
	private UsuarioService dao;
	@Autowired
	private SecurityCheck security;

	@RequestMapping({ "/editaUsuario" })
	public ModelAndView editaUsuario() {
		return new ModelAndView("editaUsuario", "usuario", new Usuario());
	}

	@RequestMapping("/salvaUsuario")
	public ModelAndView salvaUsuario(@ModelAttribute("usuario") Usuario usuario, 
			RedirectAttributes redirectAttributes) {

		if(!security.isInRole("ADMIN")) {
			ModelAndView model = new ModelAndView("index.html");
			model.addObject("mensagem", "O Acesso foi Negado");
			model.addObject("tipo.mensagem", "alert-danger");
			return model;
		} else {
			boolean editar = usuario.isEditar();
			dao.salvar(usuario);
			if(editar) {
				redirectAttributes.addFlashAttribute("mensagem", "Usuario cadastrado");
				redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
				return new ModelAndView("redirect:/editaUsuario");
			} else {
				redirectAttributes.addFlashAttribute("mensagem", "Usuario atualizado");
				redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
				return new ModelAndView("redirect:/listaUsuario");
			}
		}
	}

	@RequestMapping("/listaUsuario")
	public ModelAndView listaUsuario() {
		ModelAndView model = new ModelAndView("listaUsuario", "usuarios", dao.getUsuarios());
		return model;
	}

	@RequestMapping("/removeUsuario/{nome}")
	public String removeUsuario(@PathVariable("nome") String nome, Model model, 
			RedirectAttributes redirectAttributes) {

		if(!security.isInRole("ADMIN")) {
			model.addAttribute("mensagem", "O Acesso foi Negado");
			model.addAttribute("tipo.mensagem", "alert-danger");
			return "listaUsuario";
		} else if (dao.removeUsuario(nome)) {
			redirectAttributes.addFlashAttribute("mensagem", "Usuário excluído");
			redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
			return "redirect:/listaUsuario";
		} else {
			model.addAttribute("mensagem", "Houve falha ao excluir o Usuário");
			model.addAttribute("tipo.mensagem", "alert-danger");
			return "listaUsuario";
		}
	}

	@RequestMapping("/editaUsuario/{nome}")
	public ModelAndView editaUsuario(@PathVariable(value = "nome") String nome,
			RedirectAttributes redirectAttributes) {
		ModelAndView model;

		Usuario usuario = dao.getUsuario(nome);

		if (usuario != null) {
			model = new ModelAndView("editaUsuario", "usuario", usuario);
			usuario.setOld_nome(usuario.getNome());
			model.addObject("editar", true);
		} else {
			redirectAttributes.addFlashAttribute("mensagem", "Houve falha ao localizar o Usuário");
			redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-danger");
			model = new ModelAndView("redirect:/listaUsuario");
		}

		return model;
	}
}
