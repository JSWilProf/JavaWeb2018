package br.senai.sp.informatica.cadastro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import br.senai.sp.informatica.cadastro.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>  {
}
