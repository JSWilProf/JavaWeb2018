package br.senai.sp.informatica.cadastro.component;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class SecurityFacade implements SecurityCheck {

	@Override
	public boolean isInRole(String role) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			UserDetails user = (UserDetails)auth.getPrincipal();
			return user.getAuthorities().stream()
					.filter(priv -> priv.getAuthority().equals(role))
					.findAny()
					.isPresent();
		}
		return false;
	}

	@Override
	public String getUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			UserDetails user = (UserDetails)auth.getPrincipal();
			return user.getUsername();
		}
		return null;
	}
}
