package br.senai.sp.informatica.cadastro.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListaDeClientes {
	private List<Cliente> clientes;
	private int[] ids;
}
