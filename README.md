# Repositório da Turma Java Web 2018 Jul - Senai Informatica 1.32
## Bem Vindo
Aqui são disponibilizados os projetos Java Web, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

```
git clone git@gitlab.com:JSWilProf/JavaWeb2018.git
```

Também é possível fazer o download através do link
[Java Web 2018](https://gitlab.com/JSWilProf/JavaWeb2018)

# Ementa

## Módulo Principal (40h)

- Servlet
- Spring BOOT
- Spring MVC e Thymeleaf
- Spring Date e JPA
- Bean Validation

