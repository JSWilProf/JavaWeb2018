package br.senai.sp.informatica.cadastro;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ValidaDados")
@SuppressWarnings("serial")
public class ValidaDados extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		String nome = req.getParameter("nome");
		String endereco = req.getParameter("endereco");
		String telefone = req.getParameter("telefone");
		String email = req.getParameter("email");
		
		String header = WebUtilities.loadFile(this, "/resources/header.html");
		String footer = WebUtilities.loadFile(this, "/resources/footer.html");
		
		PrintWriter out = resp.getWriter();
		out.print(header +
				"<div id='form1'><fieldset>" +
				"<h2>Cadastrado com Sucesso</h2>" +
				"<p><b>Nome</b>: " + nome + "</p>" +
				"<p><b>Endereço</b>: " + endereco + "</p>" +
				"<p><b>Telefone</b>: " + telefone + "</p>" +
				"<p><b>E-Mail</b>: " + email + "</p>" +
				"</fieldset></div>" +
				footer);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
