<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	
	<meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="expires" content="Mon, 20 jul 2000 10:00:00 GMT">
	
	<title>Servlets</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
	<div id="wrapper">
		<header role="banner" class="group">
			<div id="logo"><a href="index.html">Dados Cadastrais</a></div>
			
		</header>

		<div id="content" role="main">
			<div id="form1">
				<fieldset>
					<h2>Cadastrado com Sucesso</h2>
					<p><b>Nome</b>: ${cliente.nome}</p>
					<p><b>Endereço</b>: ${cliente.endereco}</p>
					<p><b>Telefone</b>: ${cliente.telefone}</p>
					<p><b>E-Mail</b>: ${cliente.email}</p>
				</fieldset>
			</div>		
		
		</div>
	</div>
</body>
</html>