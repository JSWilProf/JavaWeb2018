package br.senai.sp.informatica.cadastro;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cliente {
	private String nome;
	private String endereco;
	private String telefone;
	private String email;	
}
