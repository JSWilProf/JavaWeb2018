package br.senai.sp.informatica.cadastro;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.security.provider.certpath.ResponderId;

@WebServlet("/ValidaDados")
@SuppressWarnings("serial")
public class ValidaDados extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		String nome = req.getParameter("nome");
		String endereco = req.getParameter("endereco");
		String telefone = req.getParameter("telefone");
		String email = req.getParameter("email");
		
		Cliente cliente = Cliente.builder()
				.nome(nome)
				.endereco(endereco)
				.telefone(telefone)
				.email(email)
				.build();
		
		HttpSession session = req.getSession();
		session.setAttribute("cliente", cliente);
		
		resp.sendRedirect("resultado.jsp");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
